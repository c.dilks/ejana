#ifndef EJANA_TRK_EFF_HISTOGRAMS_H
#define EJANA_TRK_EFF_HISTOGRAMS_H

#include <TFile.h>
#include <TDirectory.h>
#include <TH1D.h>
#include <TH2D.h>
#include <TCanvas.h>
#include <TTree.h>
#include <TApplication.h>

namespace trk_eff{

/**
 * This file is used as a collection of histograms and objects related to filling root tree
 */
struct Histograms
{
    void Init(TFile *file)
    {
        // 'locker' locks mutex so other threads can't interfere with TFile doing its job
        std::lock_guard<std::recursive_mutex> locker(lock);

        // create a subdirectory "hist_dir" in this file
        plugin_root_dir = file->mkdir("trk_eff");
        file->cd();         // Just in case some other root file is the main TDirectory now

        h1d_pt_primaryparticle = new TH1D("pt_primary_particle", "tranverse p", 100,0,10);
        h1d_pt_primaryparticle->SetDirectory(plugin_root_dir);

        h1d_pt_true_all = new TH1D("pt_true_all", "tranverse p", 100,0,10);
        h1d_pt_true_all->SetDirectory(plugin_root_dir);

        h1d_pt_true_reconstructible = new TH1D("pt_true_reconstructible", "tranverse p", 100,0,10);
        h1d_pt_true_reconstructible->SetDirectory(plugin_root_dir);

        h1d_pt_reco = new TH1D("pt_reco", "reco p", 100,0,10);
        h1d_pt_reco->SetDirectory(plugin_root_dir);

        h1d_pt_diff = new TH1D("pt_diff", "diff p", 100,-10,10);
        h1d_pt_diff->SetDirectory(plugin_root_dir);

        /*hD0_mass = new TH1D("D0_mass", "D0 mass", 100,1.45,2.2);
        hD0_mass->GetXaxis()->SetTitle("m_{K#pi} [GeV/c^{2}]");
        hD0_mass->SetDirectory(plugin_root_dir);

        hD0_vtx= new TH1D("D0_vtx", "D0 vtx", 100,0.,300.);
        hD0_vtx->GetXaxis()->SetTitle("vertex [#mum]");
        hD0_vtx->SetDirectory(plugin_root_dir);

        hD0_pt = new TH1D("D0_pt", "D0 pt", 100,0.,5.);
        hD0_pt->GetXaxis()->SetTitle("p_{T} [GeV]");
        hD0_pt->SetDirectory(plugin_root_dir);*/

    }

    // ---- kinematic variables---
    //    TH1D *hD0_mass, *hD0_vtx, *hD0_KaonPt,*hD0_KaonPtot,*hD0_pt,*hD0_vtxK;

    std::recursive_mutex lock;
    TTree * tree_rec_e;     // Tree to store electron related data
    TH1D* h1d_pt_primaryparticle;
    TH1D* h1d_pt_true_all;
    TH1D* h1d_pt_true_reconstructible;
    TH1D* h1d_pt_reco;
    TH1D* h1d_pt_diff;


private:

    TDirectory* plugin_root_dir;   // Main TDirectory for Plugin histograms and data

};
}

#endif //EJANA_TRK_EFF_HISTOGRAMS_H
