#include "TRandom.h"
#include "TLorentzVector.h"
#include "TVector3.h"

#include <fmt/core.h>

#include "TrackingEfficiencyProcessor.h"

#include <MinimalistModel/McTrack.h>
#include "reco/trk_fit/VertexTrackerHit.h"
#include "reco/trk_fit/VertexTrackerCluster.h"
#include "reco/trk_fit/VertexTrackerTrack.h"

using namespace fmt;

void TrackingEfficiencyProcessor::Init()
{
    // Ask service locator for parameter manager. We want to get this plugin parameters.
    auto pm = services_.Get<JParameterManager>();

    // Verbosity level. 0 = show nothing. 1 - show some. 2 - mad printer
    // SetDefaultParameter actually sets the parameter value from arguments if it is specified
    verbose_ = 0;
    pm->SetDefaultParameter("trk_eff:verbose", verbose_, "Plugin output level. 0-almost nothing, 1-some, 2-everything");

    // Setup histograms. We get current TFile from Service Locator
    histos_.Init(services_.Get<TFile>());
}


void TrackingEfficiencyProcessor::Process(const std::shared_ptr<const JEvent> &event)
{
    if(verbose_ > 1) {
        print("\n====================================\n");
        print("trk_eff::Process( event number = {} ) \n", event->GetEventNumber());
    }

    // Process raw Geant4 events if available
    uint64_t primary_particle_count = 0;
    if(event->GetFactory<jleic::GeantPrimaryParticle>("", false)) {
        auto gen_parts = event->Get<jleic::GeantPrimaryParticle>();

        if(verbose_ > 1) {
            PrintGeantPrimaryParticles(gen_parts);
        }

        for(auto particle: gen_parts) {
            pdgs_.insert(particle->pdg);
            double pt_true = particle->tot_mom*sqrt(particle->dir_x*particle->dir_x + particle->dir_y * particle->dir_y);
            histos_.h1d_pt_primaryparticle->Fill(pt_true);
            primary_particle_count++;
        }
    }

    auto hits = event->Get<VertexTrackerHit>();
    auto clusters = event->Get<VertexTrackerCluster>();
    auto reco_tracks = event->Get<VertexTrackerTrack>();
    auto mc_tracks = event->Get<minimodel::McTrack>();

    std::map<uint64_t, const minimodel::McTrack*> track_lookup;
    for (auto mc_track : mc_tracks) {
        assert(track_lookup.find(mc_track->id) == track_lookup.end());
        track_lookup[mc_track->id] = mc_track;

        auto mc_pt = mc_track->p * sqrt(mc_track->dir_x * mc_track->dir_x + mc_track->dir_y * mc_track->dir_y);
        histos_.h1d_pt_true_all->Fill(mc_pt);
    }

    for(auto reco_track : reco_tracks) {

        auto mc_track = track_lookup[reco_track->track_id];
        auto mc_pt = mc_track->p * sqrt(mc_track->dir_x * mc_track->dir_x + mc_track->dir_y * mc_track->dir_y);

        histos_.h1d_pt_reco->Fill(reco_track->p.Pt());
        histos_.h1d_pt_true_reconstructible->Fill(mc_pt);
        histos_.h1d_pt_diff->Fill(reco_track->p.Pt() - mc_pt);
    }

    if (verbose_) {

        for (auto hit : hits) {
            print("Hit: track {}: {}, {}, {}\n", hit->track_id, hit->Xpos, hit->Ypos, hit->Zpos);
        }
        for (auto track : reco_tracks) {
            print("Genfit: track {}: {}, {}, {}\n", track->track_id, track->x.x(), track->x.y(), track->x.z());
        }
        for (auto track : mc_tracks) {
            print("Geant: track {}: {}, {}, {}\n", track->id, track->vtx_x, track->vtx_y, track->vtx_z);
        }
    }
}


void TrackingEfficiencyProcessor::Finish()
{
    ///< Called after last event of last event source has been processed.
    fmt::print("Found pdgs: \n");
    for (int pdg: pdgs_) {
        fmt::print("{}, ", pdg);
    }
    fmt::print("\n");
}

void TrackingEfficiencyProcessor::PrintGeantPrimaryParticles(const vector<const jleic::GeantPrimaryParticle *>& particles) {
    for(auto particle: particles) {
        print(" {} {:<10} \n", particle->id, particle->pdg);
    }
}

