#include "Kin_Variables.h"
#include "TMath.h"
#include <stdio.h>
//==================================================================
//                  using true information
//==================================================================
int  GEN_XYQ2(double (&PFSL)[4],double (&PISL)[4],double (&PISP)[4],double &X,double &Y,double &Q2) {
    //     Calculate bjorken x, y, Q2.
    //     PFSL(4)=Final state lepton 4-momentum
    //     PISL(4)=Initial state lepton 4-momentum
    //     PISP(4)=Initial state proton 4-momentum
    //     GEN_XYQ2(PFSL,PISL,PISP,X,Y,Q2,ERROR)  (all REAL)
    //
    //    ERROR <> 0, fatal error. All outputs set to zero.
    //
    double PDOTQ, PDOTK, SMALL, Q[4];
    int I;
    SMALL=1.e-10;

    X=0.;
    Y=0.;
    Q2=0.;
    PDOTK=0.;
    PDOTQ=0.;

    for ( I=0; I<4; I++) {
        Q[I]=PISL[I]-PFSL[I];
        if(I==3) {
            Q2=Q2+Q[I]*Q[I];
            PDOTQ=PDOTQ+Q[I]*PISP[I];
            PDOTK=PDOTK+PISL[I]*PISP[I];
        } else {
            Q2=Q2-Q[I]*Q[I];
            PDOTQ=PDOTQ-Q[I]*PISP[I];
            PDOTK=PDOTK-PISL[I]*PISP[I];
        }
    }
    Q2=-1.0*Q2;

    if(fabs(PDOTK)<SMALL)    return 1;

    Y=PDOTQ/PDOTK;

    if(fabs(PDOTQ)<SMALL)    return 2;

    X=Q2/(2.0*PDOTQ);
    return 0;
}

//==================================================================
//                    Electron only method
//==================================================================
int  GEN_El_XYQ2(double &Ee,double &Eth,Double_t &EbeamE,double &EbeamP,double &X,double &Y,double &Q2) {

    double S=4*EbeamE*EbeamP;
    double CosThe=cos(Eth);
    Y=  1- (Ee*(1-CosThe))/2/EbeamE;
    Q2=2*EbeamE*Ee*(1+CosThe);

    //  Q2=(4*X*EbeamE*(EbeamE-Ee))/(EbeamE/EbeamP -X);
    X=Q2/S/Y;
    // X=EbeamE/EbeamP*(Ee*(1+Ecth)/(2*EbeamE-Ee*(1-Ecth)));
    return 0;
}
//==================================================================
//             Hadron only method (JB method )
//==================================================================
int  GEN_JB_XYQ2(double &PT2h,double &EMPZh,double &EbeamE,double &EbeamP,double &X_JB,double &Y_JB,double &Q2_JB) {

    double S=4*EbeamE*EbeamP;
    Y_JB= EMPZh/2/EbeamE;
    Q2_JB= PT2h/(1-Y_JB);
    X_JB=Q2_JB/S/Y_JB;
    return 0;
}
//==================================================================
