#include <JANA/JEventProcessor.h>
#include <JANA/JFactoryGenerator.h>


#include "DihadronProcessor.h"

/// This class is temprorary here while JANA2 API is getting to perfection.
/// Here we just list JFactories defined in this plugin
struct DihadronInputParticleGenerator:public JFactoryGenerator {
  void GenerateFactories(JFactorySet *factory_set) override {
    factory_set->Add( new JFactoryT<DihadronInputParticle>());
  }
};

extern "C"
{
  void InitPlugin(JApplication *app)
  {
    InitJANAPlugin(app);

    app->Add(new DihadronProcessor(app));
    app->Add(new DihadronInputParticleGenerator());
  }
}
