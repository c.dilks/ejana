#ifndef TOOLS_H_GUARD
#define TOOLS_H_GUARD

#include "TString.h"
#include "TRegexp.h"
#include "TMath.h"
#include "TH1.h"
#include "TH2.h"
#include "TProfile.h"
#include "TLorentzVector.h"

static const Double_t PI = TMath::Pi();
static const Double_t PIe = TMath::Pi() + 0.3;
static const Float_t UNDEF = -10000;


class Tools {
  public:

    // shift angle to the range [-PI,+PI]
    static Float_t AdjAngle(Float_t ang) {
      while(ang>PI) ang-=2*PI;
      while(ang<-PI) ang+=2*PI;
      return ang;
    };

    // shift angle to the range [0,2*PI]
    static Float_t AdjAngleTwoPi(Float_t ang) {
      while(ang>2*PI) ang-=2*PI;
      while(ang<0) ang+=2*PI;
      return ang;
    };

    // convert Eta to Theta (with theta in degrees)
    static Float_t EtaToTheta(Float_t eta) {
      return 2 * TMath::ATan( TMath::Exp( -1*eta) ) * 180.0/PI;
    };

    // convert Energy and Mass to |Momentum|
    static Float_t EMtoP(Float_t energy, Float_t mass) {
      return TMath::Sqrt( TMath::Power(energy,2) - TMath::Power(mass,2) );
    };

    // vector projection:
    // returns vA projected onto vB
    static TVector3 Project(TVector3 vA, TVector3 vB) {
      if(fabs(vB.Dot(vB))<0.0001) return TVector3(0,0,0);
      return vB * ( vA.Dot(vB) / ( vB.Dot(vB) ) );
    };

    // vector rejection: 
    // returns vC projected onto plane transverse to vD
    static TVector3 Reject(TVector3 vC, TVector3 vD) {
      if(fabs(vD.Dot(vD))<0.0001) return TVector3(0,0,0);
      return vC - Project(vC,vD);
    };

    // angle between two planes, spanned by vectors
    static Float_t PlaneAngle(
      TVector3 vA, TVector3 vB, TVector3 vC, TVector3 vD
    ) {
      TVector3 crossAB = vA.Cross(vB); // AxB
      TVector3 crossCD = vC.Cross(vD); // CxD

      Float_t sgn = crossAB.Dot(vD); // (AxB).D
      if(fabs(sgn)<0.00001) return UNDEF;
      sgn /= fabs(sgn); // sign of (AxB).D

      Float_t numer = crossAB.Dot(crossCD); // (AxB).(CxD)
      Float_t denom = crossAB.Mag() * crossCD.Mag(); // |AxB|*|CxD|
      if(fabs(denom)<0.00001) return UNDEF;
      return sgn * TMath::ACos(numer/denom);
    };


    ClassDef(Tools,1);
};

#endif
