#ifndef DIHADRON_PROCESSOR_HEADER
#define DIHADRON_PROCESSOR_HEADER

#include <thread>
#include <mutex>

#include <JANA/JEventProcessor.h>
#include <JANA/JObject.h>
#include <ejana/EServicePool.h>
#include <eicsmear/smear/Smear.h>

#include "Math/Point3D.h"
#include "Math/Vector4D.h"
#include "TRandom.h"
#include "TLorentzVector.h"
#include "TVector3.h"

#include <fmt/core.h>

#include <JANA/JEventProcessor.h>
#include <JANA/JEvent.h>

#include <MinimalistModel/McFluxHit.h>
#include <MinimalistModel/McGeneratedParticle.h>
#include <io/beagle_reader/BeagleEventData.h>

#include "DihadronRootOutput.h"
#include "Tools.h"



const Double_t MassPI = 0.139570,
               MassK = 0.493677,
               MassP = 0.93827,
               MassE = 0.000511,
               MassN = 0.939565,
               MassMU = 0.105658;

class JApplication;

class DihadronInputParticle:public JObject {
  public:
    ROOT::Math::PxPyPzMVector p;
    ROOT::Math::XYZPoint vertex;
    int pdg;
    int charge;
    int mother_id;
    int mother_pdg;
    int grand_mother_id;
    int grand_mother_pdg;
};


class DihadronProcessor : public JEventProcessor
{
  public:

    // Constructor just applies
    explicit DihadronProcessor(JApplication *app=nullptr):
      JEventProcessor(app),
      services(app)
    {};

    // This is called once before the first call to the Process method
    // below. You may, for example, want to open an output file here.
    // Only one thread will call this.
    void Init() override;

    //----------------------------
    // Process
    //
    // This is called for every event. Multiple threads may call this
    // simultaneously. If you write something to an output file here
    // then make sure to protect it with a mutex or similar mechanism.
    // Minimize what is done while locked since that directly affects
    // the multi-threaded performance.
    void Process(const std::shared_ptr<const JEvent>& event) override;

    //----------------------------
    // Finish
    //
    // This is called once after all events have been processed. You may,
    // for example, want to close an output file here.
    // Only one thread will call this.
    void Finish() override;

  private:
    DihadronRootOutput rootOutput;
    ej::EServicePool services;
    int verbose; // verbose output level

    /////////////////////////////
    // BEAM ENERGIES
    double e_beam_energy = 5;
    double ion_beam_energy = 100;
    /////////////////////////////
    
    TLorentzVector vecPbeam, vecEbeam;
    TLorentzVector vecEle, vecEleTmp, vecQ, vecW, vecMmiss;
    double eleTheta, eleE, eleETmp, eleE_smear, ionE;
    double eleEmax;
    bool eleFound;

    enum pmEnum {kP,kM};
    int h;
    TLorentzVector vecHad[2];
    TLorentzVector vecPh, vecR;
    TVector3 pQ,pL,pPh,pR;
    TVector3 pHad[2];
    TVector3 pHad_perp[2];
    TVector3 pRT;
    int hadPID[2];
    Double_t hadMass[2];

    TLorentzVector vecHad_com[2];
    TLorentzVector vecPh_com, vecQ_com;
    TVector3 pHad_com[2];
    TVector3 pPh_com, pQ_com;
    TLorentzVector boostvecCom;
    TVector3 ComBoost;




    // KINEMATIC VARS ///////////////////////////////////////
    //
    double eleX, eleY, eleQ2, eleW;
    double Xtrue, Q2true, Ytrue, W2true, Nutrue;

    Float_t PhMag; // dihadron total momentum
    Float_t PhPerpMag; // transverse component of dihadron total momentum (perp frame)
    Float_t PhEta; // pseudorapidity of dihadron pair
    Float_t PhPhi; // azimuth of dihadron pair

    Float_t RMag; // dihadron relative momentum
    Float_t RTMag; // transverse componet of relative momentum (T-frame)
    Float_t RPerpMag; // transverse componet of relative momentum (perp-frame)

    Float_t PhiH; // angle[ reaction_plane, Ph^q ]
    Float_t PhiR; // angle[ reaction_plane, RT^q ]
    Float_t z[2]; // fraction of energy of fragmenting parton
    // carried by the hadron
    Float_t zpair; // fraction of energy of fragmenting parton
    // carried by the hadron pair
    Float_t Mh; // dihadron invariant mass
    Float_t hadM[2]; // hadron mass
    Float_t Mmiss; // missing mass
    Float_t xF; // feynman-x
    Float_t hadXF[2]; // feynman-x for each hadron
    Float_t hadVertex[2][3]; // vertex
    Float_t hadChi2pid[2]; // chi2 of PID
    Int_t hadStatus[2]; // status variable

    Float_t zeta; // lab-frame energy sharing
    Float_t theta; // CoM-frame angle between Ph and P1
    Float_t MRterm[2]; // (used for computing theta)

};

#endif
