#include "DihadronProcessor.h"

using namespace std;
using namespace fmt;

void DihadronProcessor::Init() {
  // initialize class that holds histograms and other root objects
  // 'services' is a service locator, we ask it for TFile
  rootOutput.init(services.Get<TFile>());
  rootOutput.dihTree->Branch("Mh",&Mh);
  rootOutput.dihTree->Branch("eleQ2",&eleQ2);
  rootOutput.dihTree->Branch("eleX",&eleX);
  rootOutput.dihTree->Branch("eleY",&eleY);
  rootOutput.dihTree->Branch("eleW",&eleW);
  rootOutput.dihTree->Branch("z",z,"z[2]/F");
  rootOutput.dihTree->Branch("zpair",&zpair);
  //rootOutput.dihTree->Branch("xxx",&xxx);

  // Ask service locator for parameter manager. We want to get this plugin parameters.
  auto pm = services.Get<JParameterManager>();

  // Verbosity level. 0 = show nothing. 1 - show some. 2 - mad printer
  // SetDefaultParameter actually sets the parameter value from arguments if it is specified
  verbose = 0;
  pm->SetDefaultParameter("dihadron:verbose", 
    verbose, "Plugin output level. 0-almost nothing, 1-some, 2-everything");
  if(verbose) print("[+] call DihadronProcessor::Init()\n");

  // Beams energies
  pm->SetDefaultParameter("dihadron:e_beam_energy", e_beam_energy, "Energy of colliding electron beam");
  pm->SetDefaultParameter("dihadron:ion_beam_energy", ion_beam_energy, "Energy of colliding ion beam");
  if(verbose) {
    print("Parameters:\n");
    print("  dihadron:verbose:         {}\n", verbose);
    print("  dihadron:e_beam_energy:   {}\n", e_beam_energy);
    print("  dihadron:ion_beam_energy: {}\n", ion_beam_energy);
  };
}


void DihadronProcessor::Process(const std::shared_ptr<const JEvent> &event) {

  using namespace fmt;
  using namespace std;
  using namespace minimodel;
  std::lock_guard<std::recursive_mutex> locker(rootOutput.lock);
  
  vecPbeam.SetXYZM(ion_beam_energy * sin(0.05), 0., ion_beam_energy * cos(0.05), MassP);
  vecEbeam.SetXYZM(e_beam_energy, 0., -e_beam_energy, MassE);

  // >oO debug printing
  if(verbose >= 2) print("[===========] NEXT EVENT {}\n ",event->GetEventNumber());

  // get beagle event (truth)
  /*
  auto beagleEvent = event->GetSingle<ej::BeagleEventData>();
  if(verbose >= 2) print("Beagle event data: {} {} \n",
    beagleEvent->truex,
    beagleEvent->trueQ2);
  */

  // get generated particles
  auto genParticles = event->Get<DihadronInputParticle>();
  
  // loop over electrons, to find scattered electron
  eleFound = false;
  eleEmax = 0;
  for(auto ele : genParticles) {
    if(ele->pdg == 11) {

      vecEleTmp.SetXYZM(ele->p.px(), ele->p.py(), ele->p.pz(), MassE);
      eleETmp = vecEleTmp.E();

      if( eleETmp > eleEmax ) {
        vecEle.SetXYZM(ele->p.px(), ele->p.py(), ele->p.pz(), MassE);
        eleE = vecEle.E();
        eleFound = true;
        eleEmax = eleE;

        eleTheta = ele->p.Theta();
        ionE = 2. * ion_beam_energy * cos(0.05);

        // eleE smearing
        eleE_smear = eleE;
        // gRandom->Gaus(eleE, 0.1*sqrt(eleE) + 0.01*eleE + 0.005);

        // calculate x,y,Q2 // TODO do we need this here?
        //GEN_El_XYQ2(eleE_smear, eleTheta, e_beam_energy, ionE, eleX, eleY, eleQ2); 

      };
    };
  }; // eo electron loop


  if(!eleFound) { print("[+] no scattered e-\n"); return; };
  // get truth values from beagle
  /*
  Xtrue = beagleEvent->truex;
  Q2true = beagleEvent->trueQ2;
  Ytrue = beagleEvent->truey;
  W2true = beagleEvent->trueW2;
  Nutrue = beagleEvent->trueNu;
  */
  // calculate DIS kinematics from scattered electron
  vecW = vecEbeam + vecPbeam - vecEle;
  vecQ = vecEbeam - vecEle;
  eleW = vecW.M();
  eleQ2 = -1*vecQ.M2();
  eleX = eleQ2 / ( 2 * vecQ.Dot(vecPbeam) );
  eleY = vecPbeam.Dot(vecQ) / vecPbeam.Dot(vecEbeam);
  // calculate boost vectors
  boostvecCom = vecQ + vecPbeam;
  ComBoost = -1*boostvecCom.BoostVector();

  if (verbose >= 2) {
    print(">>>>>>>>>>>>>>>>> electron found <<<<<<<<<<<<<<<<<<<<<<<<<<<\n");
    print(" Eebeam {}  Epbeam {} \n", vecEbeam.E(), vecPbeam.E());
    print(" xtrue {} xel {}  Q2true {} eleQ2 {} ytrue {} yel {} W2_true {} Nu_True {}  t=={} \n ",
        Xtrue, eleX, Q2true, eleQ2, Ytrue, eleY, W2true, Nutrue, UNDEF/*beagleEvent->t_hat*/);
  }

  
  // dihadron loop
  hadPID[kP] = 211;
  hadPID[kM] = -211;
  for(h=0; h<2; h++) hadMass[h] = MassPI;
  for(auto had0 : genParticles) {
    if(had0->pdg == hadPID[0]) {
      for(auto had1 : genParticles) {
        if(had1->pdg == hadPID[1]) {
          
          vecHad[0].SetXYZM(had0->p.px(), had0->p.py(), had0->p.pz(), hadMass[0]);
          vecHad[1].SetXYZM(had1->p.px(), had1->p.py(), had1->p.pz(), hadMass[1]);
          
          // compute 4-momenta Ph and R
          vecPh = vecHad[0] + vecHad[1];
          vecR = 0.5 * ( vecHad[0] - vecHad[1] );
          
          // get 3-momenta from 4-momenta
          pQ = vecQ.Vect();
          pL = vecEle.Vect();
          pPh = vecPh.Vect();
          pR = vecR.Vect();
          for(h=0; h<2; h++) pHad[h] = vecHad[h].Vect();

          // compute z
          for(h=0; h<2; h++) z[h] = vecPbeam.Dot(vecHad[h]) / vecPbeam.Dot(vecQ);
          zpair = vecPbeam.Dot(vecPh) / vecPbeam.Dot(vecQ);

          // compute invariant mass (and hadron masses)
          Mh = vecPh.M();
          for(h=0; h<2; h++) hadM[h] = vecHad[h].M();

          // compute missing mass
          vecMmiss = vecW - vecPh;
          Mmiss = vecMmiss.M();

          // compute xF (in CoM frame)
          vecPh_com = vecPh;
          vecQ_com = vecQ;
          vecPh_com.Boost(ComBoost);
          vecQ_com.Boost(ComBoost);
          pPh_com = vecPh_com.Vect();
          pQ_com = vecQ_com.Vect();
          for(h=0; h<2; h++) {
            vecHad_com[h] = vecHad[h];
            vecHad_com[h].Boost(ComBoost);
            pHad_com[h] = vecHad_com[h].Vect();
          };
          xF = 2 * pPh_com.Dot(pQ_com) / (eleW * pQ_com.Mag());
          for(h=0; h<2; h++)
            hadXF[h] = 2 * pHad_com[h].Dot(pQ_com) / (eleW * pQ_com.Mag());

          // compute theta
          zeta = 2 * vecR.Dot(vecPbeam) / ( vecPh.Dot(vecPbeam) );
          for(h=0; h<2; h++) MRterm[h] = TMath::Sqrt( vecHad[h].M2() + pR.Mag2() );
          theta = TMath::ACos(  ( MRterm[0] - MRterm[1] - Mh*zeta ) / ( 2*pR.Mag() )  );

          // compute PhiH
          PhiH = Tools::PlaneAngle(pQ,pL,pQ,pPh);

          // compute PhiR
          // - TODO: double check this PhiR definition is the preferred one
          for(h=0; h<2; h++) pHad_perp[h] = Tools::Reject(pHad[h],pQ);
          pRT = (1.0/zpair) * ( z[1]*pHad_perp[0]  -  z[0]*pHad_perp[1] );
          PhiR = Tools::PlaneAngle(pQ,pL,pQ,pRT);

          /*if(Mmiss>1.05 && zpair<0.95 && z[0]>0.1 && z[1]>0.1)*/ rootOutput.dihTree->Fill();
        }; // eo if had1 is pi-
      }; // eo had1 loop
    }; // eo if had0 is pi+
  }; // eo had0 loop

}


void DihadronProcessor::Finish() {
  if(verbose) print("[+] call DihadronProcessor::Finish()\n");
}


// template specialization for input particles
template<>
void JFactoryT<DihadronInputParticle>::Process(const std::shared_ptr<const JEvent> &event) {
  using namespace fmt;
  using namespace std;
  using namespace minimodel;
  static bool said_once = false;


  std::vector<const minimodel::McGeneratedParticle *> gen_parts;

  // SMEARING
  bool smeared_taken = event->GetFactory<McGeneratedParticle>("smear", false);
  if(smeared_taken) gen_parts = event->Get<McGeneratedParticle>("smear");
  else              gen_parts = event->Get<McGeneratedParticle>();
  if(!said_once) {
    said_once = true;
    print("JFactoryT<DihadronInputParticle> Smeared taken = {}", smeared_taken);
  }

  std::vector<DihadronInputParticle *> input_parts;
  for (auto gen_part : gen_parts) {
    if (!gen_part->is_stable) continue;      // skip not final state particles
    auto input_part = new DihadronInputParticle();
    input_part->pdg = gen_part->pdg;
    input_part->p = ROOT::Math::PxPyPzMVector(gen_part->px, gen_part->py, gen_part->pz, gen_part->m);
    input_part->vertex.SetXYZ(gen_part->vtx_x, gen_part->vtx_y, gen_part->vtx_z);
    input_parts.push_back(input_part);
  }

  Set(std::move(input_parts));
}
