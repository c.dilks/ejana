#ifndef DIHADRON_ROOT_OUTPUT_HEADER
#define DIHADRON_ROOT_OUTPUT_HEADER

#include <TFile.h>
#include <TDirectory.h>
#include <TH1D.h>
#include <TH2D.h>
#include <TCanvas.h>
#include <TTree.h>
#include <TApplication.h>

class DihadronRootOutput
{
  public:

    ///////////////////
    void init(TFile *file)
    {
      // 'locker' locks mutex so other threads can't interfere with TFile doing its job
      std::lock_guard<std::recursive_mutex> locker(lock);        

      // create a subdirectory "hist_dir" in this file
      plugin_root_dir = file->mkdir("dihadron");
      file->cd();         // Just in case some other root file is the main TDirectory now

      dihTree = new TTree("dihTree", "dihadron tree");
      dihTree->SetDirectory(plugin_root_dir);

      // instantiate histograms here
      /* example:
      h2_XQ2_em_cuts = new TH2I("h2_XQ2_em_cuts ","El X,Q2 cuts ", XBINS_h,xEdges_h, Q2BINS_h, Q2Edges_h);
      h2_XQ2_em_cuts->SetDirectory(plugin_root_dir);
      h2_XQ2_em_cuts->GetXaxis()->SetTitle("log(x)");
      h2_XQ2_em_cuts->GetYaxis()->SetTitle("log(Q^{2})");
      */

    } // eo init
    ///////////////////

    std::recursive_mutex lock;
    TTree * dihTree;     // Tree to store electron related data

    // declare histograms pointers here


  private:
    TDirectory * plugin_root_dir;   // Main TDirectory for Plugin histograms and data
};

#endif
