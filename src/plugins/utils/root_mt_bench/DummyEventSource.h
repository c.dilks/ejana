#ifndef EJANA_DUMMYEVENTSOURCE_H
#define EJANA_DUMMYEVENTSOURCE_H


#include <fstream>

#include <JANA/JEventSource.h>
#include <JANA/JEvent.h>
#include <JANA/JSourceFactoryGenerator.h>

#include <TRandom.h>

#include <ejana/EServicePool.h>
#include <ejana/EventSourceControl.h>




class DummyEventData: public JObject {
public:
    explicit DummyEventData(double d) { random_value = d; }

    double random_value;
};

class DummyEventDataOther: public JObject {
public:
    explicit DummyEventDataOther(double d) { random_value = d; }

    double random_value;
};


class DummyEventSource: public JEventSource {
public:

    explicit DummyEventSource(const std::string& source_name, JApplication *app = nullptr);
    ~DummyEventSource() override  = default;

    /** Get description of this event source */
    std::string GetVDescription() const override {return GetDescription(); }

    // A description of this source type must be provided as a static member
    static std::string GetDescription() { return "Empty event source"; }

    /** Reads next event and returns it as JEvent from */
    void GetEvent(std::shared_ptr<JEvent>) override;

    /** Provides a factory with objects */
    bool GetObjects(const std::shared_ptr<const JEvent>&, JFactory* ) override {return false;}

protected:
    int64_t entry_index;			        // int64 but not uint64 because of the root GetEntries

private:
    ej::EventSourceControl* eventSourceControl;
};



//----------------
// Constructor
//----------------
inline DummyEventSource::DummyEventSource(const std::string& source_name, JApplication *app):
        JEventSource(source_name, app)
{
    using namespace fmt;

    // Open file
    print("JEventSource_beagle: Opening TXT file {} !\n", source_name);

    // Event index = number of event read
    entry_index = 0;

    eventSourceControl = new ej::EventSourceControl(app->GetJParameterManager());
    print("EventSourceControl: created \n");

    //Make factory generator that will make factories for all types provided by the event source
    //This is necessary because the JFactorySet needs all factories ahead of time
    //Make sure that all types are listed as template arguments here!!
    SetFactoryGenerator(new JSourceFactoryGenerator<DummyEventData>());

}


//----------------
// GetEvent
//----------------
inline void DummyEventSource::GetEvent(std::shared_ptr<JEvent> event)
{
    // Read an event from the source and copy the vital info into the Geant4EicEventData structure.
    auto decision = eventSourceControl->Decide(entry_index);
    while(decision == ej::EventControlDecisions::Skip) {
        entry_index++;
        decision = eventSourceControl->Decide(entry_index);
    }

    if (decision == ej::EventControlDecisions::Stop){
        throw JEventSource::RETURN_STATUS::kNO_MORE_EVENTS;
    }
    else {
        // Parsing this event
        entry_index++;
        event->Insert(new DummyEventData(gRandom->Gaus(0, 0.1)));
    }
}


/// Produces McGeneratedParticle
template<>
inline void JFactoryT<DummyEventDataOther>::Process(const std::shared_ptr<const JEvent>& event) {

    auto beagle_particles = event->Get<DummyEventData>();

    Insert(new DummyEventDataOther(gRandom->Gaus(0, 0.1)));
}



#endif //EJANA_DUMMYEVENTSOURCE_H
