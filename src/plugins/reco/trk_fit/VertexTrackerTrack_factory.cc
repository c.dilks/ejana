
#include "VertexTrackerTrack_factory.h"
#include "VertexTrackerCluster.h"

#include <JANA/JEvent.h>


void VertexTrackerTrack_factory::Process(const std::shared_ptr<const JEvent>& event) {

    EicGenfitInput input;
    input.clusters = event->Get<VertexTrackerCluster>();
    input.truths = event->Get<minimodel::McTrack>();
    input.interactive = event->GetJApplication()->GetParameterValue<bool>("trk_fit:interactive");

    genfit_results = FitTracks(input);

    Set(genfit_results.tracks);
}


