#ifndef _EICGEN_FIT_H_
#define _EICGEN_FIT_H_

// From Genfit
#include <MeasurementCreator.h>
// #include <GFRaveVertexFactory.h>
#include <AbsKalmanFitter.h>
#include <EventDisplay.h>

// From ROOT
#include <TVector3.h>

#include <vector>
#include <math.h>

class DMagneticFieldMap;

struct trkHit_t {
    float x, y, z;       ///< point in lab coordinates
    float phi_circle;    ///< phi angle relative to axis of helix
};


class eicGenFit {
public:
    eicGenFit();

    ~eicGenFit();

    void FitTrack();

    void AddHitXYZ(float x, float y, float z);

    void Clear();

    TVector3 pos3;
    TVector3 mom3;
    double trkChi2;
    int trkNdf;
    int rtrkCharge;

protected:
    std::vector<trkHit_t*> hits;

    genfit::MeasurementCreator measurementCreator;

    genfit::EventDisplay* display;

    genfit::AbsKalmanFitter* fitter;

    //genfit::GFRaveVertexFactory vertexFactory;

    bool verbose = false;
};


#endif //_EICGEN_FIT_H_
