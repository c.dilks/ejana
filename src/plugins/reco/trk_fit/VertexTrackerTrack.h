// $Id$
//
//    File: VertexTrackerTrack.h
// Created: Thu Jan  5 21:58:34 EST 2017
// Creator: davidl (on Darwin harriet 15.6.0 i386)
//

#ifndef _VertexTrackerTrack_
#define _VertexTrackerTrack_

#include <JANA/JObject.h>

#include <TVector3.h>

class VertexTrackerTrack:public JObject{
	public:
		JOBJECT_PUBLIC(VertexTrackerTrack);
	    TVector3 x;
		TVector3 p;
		double theta;
		double charge;
		double phi;
		int    q;
		double chisq;
		double ndf;
		uint64_t track_id;  // For use with MC
};

#endif // _VertexTrackerTrack_

