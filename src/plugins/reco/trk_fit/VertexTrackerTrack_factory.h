// $Id$
//
//    File: VertexTrackerTrack_factory.h
// Created: Thu Jan  5 21:58:34 EST 2017
// Creator: davidl (on Darwin harriet 15.6.0 i386)
//

#ifndef _VertexTrackerTrack_factory_
#define _VertexTrackerTrack_factory_

#include <JANA/JFactoryT.h>
#include "VertexTrackerTrack.h"

#include "EicGenfitWrapper.h"


class VertexTrackerTrack_factory : public JFactoryT<VertexTrackerTrack> {

public:
    void Process(const std::shared_ptr<const JEvent>& event) override;

    //EicGenfitWrapper genfit_wrapper; // TODO: Bring this back as a JService

    EicGenfitOutput genfit_results;
};

#endif // _VertexTrackerTrack_factory_

