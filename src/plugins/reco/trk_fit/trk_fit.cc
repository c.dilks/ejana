#include <JANA/JFactoryGenerator.h>
#include <JANA/JApplication.h>
#include <JANA/JFactoryT.h>

#include "VertexTrackerHit.h"
#include "VertexTrackerCluster.h"
#include "VertexTrackerTrack_factory.h"
#include "EventDisplayProcessor.h"

class JFactoryGenerator_vtx_fit:public JFactoryGenerator{
public:

    void GenerateFactories(JFactorySet *factory_set) override {

        factory_set->Add(new JFactoryT<VertexTrackerHit>());
        factory_set->Add(new JFactoryT<VertexTrackerCluster>());
        factory_set->Add(new VertexTrackerTrack_factory());
    }
};


// Routine used to create our JEventProcessor
extern "C"{
    void InitPlugin(JApplication *app){
        InitJANAPlugin(app);
        app->Add(new JFactoryGenerator_vtx_fit());
        app->Add(new EventDisplayProcessor(app));
    }
} // "C"
