//- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
// Jefferson Science Associates LLC Copyright Notice:
//
// Copyright 251 2014 Jefferson Science Associates LLC All Rights Reserved. Redistribution
// and use in source and binary forms, with or without modification, are permitted as a
// licensed user provided that the following conditions are met:
// 1. Redistributions of source code must retain the above copyright notice, this
//    list of conditions and the following disclaimer.
// 2. Redistributions in binary form must reproduce the above copyright notice, this
//    list of conditions and the following disclaimer in the documentation and/or other
//    materials provided with the distribution.
// 3. The name of the author may not be used to endorse or promote products derived
//    from this software without specific prior written permission.
// This material resulted from work developed under a United States Government Contract.
// The Government retains a paid-up, nonexclusive, irrevocable worldwide license in such
// copyrighted data to reproduce, distribute copies to the public, prepare derivative works,
// perform publicly and display publicly and to permit others to do so.
// THIS SOFTWARE IS PROVIDED BY JEFFERSON SCIENCE ASSOCIATES LLC "AS IS" AND ANY EXPRESS
// OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
// MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED.  IN NO EVENT SHALL
// JEFFERSON SCIENCE ASSOCIATES, LLC OR THE U.S. GOVERNMENT BE LIABLE TO LICENSEE OR ANY
// THIRD PARTES FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
// DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS
// OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF
// LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR
// OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
// POSSIBILITY OF SUCH DAMAGE.
//- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
//
// Author: Nathan Brei
//

#ifndef EJANA_EICGENFITWRAPPER_H
#define EJANA_EICGENFITWRAPPER_H


#include <MinimalistModel/McTrack.h>

#include "VertexTrackerTrack.h"
#include "VertexTrackerCluster.h"

#include <vector>


struct EicGenfitInput {

    std::vector<const VertexTrackerCluster*> clusters;
    std::vector<const minimodel::McTrack*> truths;

    bool interactive = false;
    bool use_rave = false;
    int debug_level = 0;
    int det_id = 1;
    double y_res = 0.0001; // cm?
    double t_res = 0.0001; // TODO: Resolution keyed off of volume name
    std::set<int> pdgs {11, 12, 13, 14, 22, 113, 130, 211, 321, 333, 443, 2112, 2212};  // TODO: Get this from event

};

struct EicGenfitOutput {

    std::vector<VertexTrackerTrack*> tracks;
    uint64_t attempted_count = 0;
    uint64_t succeeded_count = 0;
    uint64_t insufficient_data_count = 0;
    uint64_t exception_count = 0;
    uint64_t misfit_count = 0;

};

EicGenfitOutput FitTracks(const EicGenfitInput&);


#endif //EJANA_EICGENFITWRAPPER_H
