#ifndef EJANA_JLEICSMEAR_H
#define EJANA_JLEICSMEAR_H

#include <MinimalistModel/McGeneratedParticle.h>

void SmearPrticleJleic(minimodel::McGeneratedParticle *particle, int _verbose);

#endif //EJANA_JLEICSMEAR_H
