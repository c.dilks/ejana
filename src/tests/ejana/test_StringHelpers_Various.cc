#include <string>

#include <catch.hpp>
#include <ejana/EStringHelpers.h>

TEST_CASE( "Simple operations tests", "[StringHelpers]" ) {
    REQUIRE(ej::EndsWith("Horosho", "sho"));

    REQUIRE(ej::StartsWith("==============", "=="));

    std::string str_to_trim("  haha\t");

    REQUIRE(ej::TrimCopy(str_to_trim) == "haha");

    ej::TrimThis(str_to_trim);
    REQUIRE(str_to_trim == "haha");

}

