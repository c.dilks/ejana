///
/// This is the simplest unit tests.
/// Also the definition CATCH_CONFIG_MAIN means that Catch Unit Test framework will generate 'main' function.
/// This define should be placed only once among all files wich contain tests
/// See more on the catch test framework here.
/// https://github.com/philsquared/Catch2
///
///==========================================================================================================

#define CATCH_CONFIG_MAIN  // This tell CATCH to provide a main() - only do this in one cpp file
//#define CATCH_CONFIG_RUNNER

#include "catch.hpp"
#include <stdlib.h>
#include <string>

