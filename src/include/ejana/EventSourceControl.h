//
// Created by Alexander on 15.07.2019.
//

#ifndef EJANA_EVENTSOURCECONTROL_H
#define EJANA_EVENTSOURCECONTROL_H

#include <JANA/Services/JParameterManager.h>

namespace ej{
    enum class EventControlDecisions{
        Skip,
        Process,
        Stop
    };

    class EventSourceControl {
    public:
        EventSourceControl(JParameterManager * parameterManager) {
            nevents_to_process = parameterManager->GetParameterValue<uint64_t>("nevents");
            nevents_to_skip = parameterManager->GetParameterValue<uint64_t>("nskip");
        }

        EventControlDecisions Decide(uint64_t event_number) {
            if (event_number < nevents_to_skip) {
                return EventControlDecisions::Skip;
            }
            //if nevents_to_process == 0 then process all events until the end
            if (nevents_to_process && event_number >= nevents_to_skip + nevents_to_process) {
                return EventControlDecisions::Stop;
            }
            else return EventControlDecisions::Process;
        }

    private:
        uint64_t nevents_to_process;
        uint64_t nevents_to_skip;
    };

}

#endif //EJANA_EVENTSOURCECONTROL_H
