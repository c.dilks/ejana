#ifndef _EJanaRootApplication_H_
#define _EJanaRootApplication_H_


#include <TF1.h>
#include <TFile.h>

#include <JANA/JApplication.h>



namespace ej {

	class EJanaRootApplication: public JApplication {

	/// The EJanaRootApplication class extends the JApplication class
	/// by adding some specific data, default factories and etc.

	public:
		EJanaRootApplication(JParameterManager* pm):
			JApplication(pm)
        {
            // disable inherently (and horrorifically)-unsafe registration of EVERY TObject with the global TObjectTable //multithreading!!
            // simply setting/checking a bool is not thread-safe due to cache non-coherence and operation re-shuffling by the compiler
            TObject::SetObjectStat(kFALSE);

            // Similar: This adds functions to the global gROOT.  We don't want this, because different threads tend to have their own functions with the same name.
            TF1::DefaultAddToGlobalList(kFALSE);

            // Root output file
            std::string out_name = pm->GetParameterValue<std::string>("output");
            root_outputFile = new TFile(out_name.c_str(), "RECREATE");

            pm->SetDefaultParameter("nevents", event_sources_nevents, "Obsolete. Number of events to process. 0 = process all. See also 'nskip'");
            pm->SetDefaultParameter("nskip", event_sources_nskip, "Obsolete. Number of events to skip. See also 'nevents'");
        }

		virtual ~EJanaRootApplication()
		{
			// Kill what we made
			if(root_outputFile && root_outputFile->IsOpen() && !root_outputFile->IsZombie()) {
				// Write file
                root_outputFile->Write("+");
			}

			delete root_outputFile;
		}


		/// Returns the "main" root output file for EJana
		TFile * get_output_root_file() const { return  root_outputFile; }


	protected:
	    uint64_t event_sources_nevents = 0;
		uint64_t event_sources_nskip = 0;

	private:
		TFile * root_outputFile;	// The "main" root ouput file
	};
}

#endif // _EJanaRootApplication_H_

