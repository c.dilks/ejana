#ifndef EJANA_EJOURNAL_H
#define EJANA_EJOURNAL_H

#include <vector>

#include "EPage.h

namespace ej {
    class EJournal {
        friend EPage;
    public:

        EPage& GetPage(std::string name);

        EPage& GetPage(uint64_t index);


        std::vector<EPage>& Pages();
        std::map<std::string, EPage> GetPagesByName();

    private:
        std::vector<EPage> pages_;
        std::map<std::string, EPage> pages_by_name_;
    };
}
#endif //EJANA_EJOURNAL_H
