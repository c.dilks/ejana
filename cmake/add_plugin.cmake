# Common macro to add plugins
macro(add_plugin _name)
    add_library(${_name} SHARED "")
    target_include_directories(${_name}
            INTERFACE
            ${PROJECT_SOURCE_DIR}/src/include

            PUBLIC
            ${PROJECT_SOURCE_DIR}/src/plugins/
            ${PROJECT_SOURCE_DIR}/src/plugins/${_name}/)

    set_target_properties(${_name} PROPERTIES LIBRARY_OUTPUT_DIRECTORY "${PLUGIN_OUTPUT_DIRECTORY}")

    # we don't want our plugins to start with 'lib' prefix
    # example: we want vmeson.so but not libvmeson.so
    set_target_properties(${_name} PROPERTIES PREFIX "")

    install(TARGETS ${_name}
            DESTINATION plugins
            PUBLIC_HEADER DESTINATION include/plugins)
endmacro()